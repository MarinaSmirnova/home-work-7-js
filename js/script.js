const myArr = ['hello', 'world', 23, '23', null];

function filterBy(arr, type) {

  return arr.filter(item => typeof item !== type);

};

let type = prompt("Enter the type of data you want to filter (number, string, boolean, undefined, object, bigint, function, symbol)");

console.log(filterBy(myArr, type));